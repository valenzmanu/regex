# Regex

This is a host for the Regex library by Nick Gammon.

* Original Library: http://gammon.com.au/Arduino/Regexp.zip (41Kb).
* Documentation: http://www.gammon.com.au/scripts/doc.php?lua=string.find
